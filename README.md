# Overview

Alpine based mysql (mariadb) image and custom entrypoint script
that forked from the official mysql-server image which: 

- uses mysq_install_db to populate /var/lib/mysql because alpine's 
mariadb version doesn't support new --initialize-insecure option yet.
- Starts mysql service with su-exec instead of gosu.
- Generates mysql.cnf file dynamically based on the memory limits of 
the container.
- MYSQL_*_FILE variables support for docker swarm secrets feature.

**Start a mysql server instance**

```
docker run --name some-mysql -e MYSQL_ROOT_PASSWORD=my-secret-pw -d alpine-mysql:tag
```

**Note:** It does not mount any host directory or create data volume for db data
since we prefer to manage data persistency via docker-compose but you can set a VOLUME 
parameter in Dockerfile to point /var/lib/mysql


It also has docker swarm secret support which you can supply your credentials as a secret via
MYSQL_USER, MYSQL_ROOT_PASSWORD_FILE, MYSQL_PASSWORD and MYSQL_DATABASE variables.

Let's say you have a secret within your docker swarm cluster named mysql_root_password then you 
can create your mysql service as follow:

```
docker service create --name mysql-server \
 -e MYSQL_ROOT_PASSWORD_FILE=/run/secrets/mysql_root_password
  ...
  ...
  ...
 secostech/alpine-mariadb
```

So the container reads /run/secrets/mysql_root_password file - which is supplied to service as a secret -
and sets its content as MYSQL_ROOT_PASSWORD variable.

For all other usage examples see https://hub.docker.com/_/mysql/